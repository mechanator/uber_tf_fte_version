 Uber Team Fortress - Open Sourced from:

 Mega Team Fortress, Coop vs monsters, added 4 player deathmatch, with or without bots, battle royale mode. 12-2019

 Author: Avirox (a.k.a. XavioR) up to 1-2015 with many others.

 Special thanks to Ratbert fo rguidance and some source from previous mods.

 Original MegaTeam Fortress Programmers: Ambush, Plexi, mod3m, Plasticity 1997

Additional addons by Ratbert, Argonator, Agent, Pablo, Cloud, Doc 2010-2019, Often, Aberrant.

Credits also to Hipnotic, Mega Team Fortress PTY LTD, Gold, Sh1thead. 

Special thanks to Kevin(server admin), Shark(for putting up with me), Alissa(original MEGATF CE code, deadly sniper).

Special thanks to Human[RUS](lotric) for remote testing server and hosting for bug sweeping.
Cheers to fortressone.org for breathing new life into the client and server engines.
Added painkeep weapons and impulse bindings from pk666 thanks to NIB and Shithead (SH), DOC (painkeep).
Beartraps, gravity well, can of pork n beans, auto sentry guns added.  

Original credit for Painkeep from Team Evolve. (also available for Quake 3). It's public domain now. 2015

cmd changeteam blue, red, green, and yellow always enabled. You can fight alongside the monsters or summon more.

Removed sounds for taunts, yells.

Added two whole pounds of semicolons at the end of functions to not spazz out modern compilers. -arg

It worked ok in 2008-2015 for that old compiler, but not by today's standards.

Fixed grenade allocation at spawn based on 3 X ammo attribute with a minimum of 3 of each type.

Now you can plant a prox at even newb levels regardless of ammo pack settings within each map.

Edited monster summon time, damage, quantity based on attribute summon.

Fixed the attribute saving feature for each player name logged on.

It's important to remember your Quake name with special characters.

"cmd telem" has been modified for survival mode to enabled.

In survival mode, if you get disconnected or dropped, you can "cmd beg" to be let back in because,

sometimes other players can't find or see your corpse. Or your player died and fell into an unobtainable area.

Even if you use the resurrection cube, sometimes the player would spawn and just die again. :{

Updated gibs to be tossed out (pk666), upgraded shotgun damage for tough monsters/players.

Added splash effects for dropped entities/models into lava and water. addtosplash() .

Fixed missing precached models, sounds, added VWEP support for Painkeep and Mega TF weapons.

Typos and grammar of text notifications cleaned by unknown, and argonator.

Addons and variables used by some modules that are commented out but need to be declared after defs.pqc

Also added on painkeep vars for their modules of .qc code for functions.

Use "cmd changeteam (color)" to join any 4 teams. You can load any non-coop map and just frag or CTF.

You can even CTF/deathmatch with "coop" maps by not loading the .ENT file for that map.

Regular deathmatch is possible by just loading a map without monsters.

The medic class can heal with masters skill "cmd aura" when spawned.							

Quirks:

Summoned monsters might only target the RED team if monster maps are selected.

Beartraps stick where you place them and cannot be shot off you once clamped.

Beartraps will follow you through the teleporters once clamped.
Regular sentry guns and tesla coils envy the auto sentry guns and will target them as an enemy on your team.

This prevent someone just ganging a huge armada of sentry guns of both types along with the

Auto sentry guns are waterproof and cannot be toastered.

Removed the following monsters to free up precache for sounds and models:

Ogre cook, Vermis(lame), eel,and gremlin which didn't work.



BUGS:		
New bugs can be submitted to Gitlab for tracking and resolution unbiased.
Volunteer devs of code/fixes/features gladly accepted.


								TODO:

	Clean up source code for easy compiliation in fteccgui and possibly 64bit versions. 99%

Done. Use the 32bit compiler, you can run it with fteqwsv 64 or 32 bit version.

- Down to 4 warnings on ftegccui compiler in Windows.  It's the grappling hook and it still works.

Obtain source of mvdsv that will compile with newer versions with same xavior/argonator tweaks.

Made the auto sentry guns and beartraps do Quad damage when that player is buffed with quad. -auto sentry done -arg

Add Painkeep explosive shotgun ammo when you pick up a pack from monsters. A few shells with rekt most monsters in a few shots.

If possible upgrade the lightning gun to the painkeep chain lightning gun.

Upgraded medic nail gun to lava nails ala rogue. - Set to 2.5 damage per nail now -arg

Adjust starting armor and speeds to rebalance things.

Add another weapon to the Pyro for even destructive backup. multi pyro rockets?

partial - sped up flame rockets and flamethrower. -arg 3/2020

Add Ratbert's nuke grenades to drop4 for demoman or pyro.  ?

Add Prozac/Ratbert's mini sentry guns of differing types. Some monsters/players are affected more
by plasma or fire damage than blast/nail damage.

Tame down the survival mode with less extra monster spawns to stay under the 512 entities limit.

Then again, the map makers should be aware of this and not use so many spawns.

Finish HALF-LIFE support. (A long march if asked for)

Add the Air fist weapon ala pain keep. 

					Changes to existing classes:

GOAL - Make it tougher to last to capture the flag with faster action, heinous

 amounts of weapons. Forcing the team to play as a team, not solo runners or rambos

that take advantage of the bunny hop.

All classes spawn with a minimum of 3 type 1, and 3 type 2 grenades.

If a map doesn't have ammo bags that give grenades, fix the maps.

The amount of additional grenades is 3X your ammo attribute gained from experience.

All classes get bonus health, armor, ammo, reload time, summoning etc from the 

attribute system based on experience points gain for frags.

At level 80 you get the Xav Needler gun and lightning gun weapons. Bringover from Mega COOP.

Achievement system similar to Xbox for bragging rights just like MegaTF COOP was.

Scout has a bat for the axe, jetpack uses less cells, a bit more fire damage.

Sniper had little changed. Already powerful and deadly enough. 

Soldier. The attributes system can boost this class to very powerful levels.

Demoman. Sticky pipebombs and laser trip bombs added.

Medic can heal with cmd aura regardless of level, further than players can with attribute of masters of healing.

Medic gets an upgrade of the super nailgun with 2.5 X "lava" nails.

Heavy Weapons Guy can spool up assault cannon a bit faster, and uses less ammo per shot on 20mm mode.

Pyro can fire faster flame rockets, has a chainsaw, and gets results.

Spy has a grappling hook that uses less cells, and can "poon" enemies for gibs.

Engineer can launch drones in a shorter timeframe, does a bit more damage per beam.

The reaper drone explodes like a detpack after crash landing.

Engineer gets a gravity gun, and can have a Tesla and Sentry built at the same time.

All classes can buy weapon upgrades, powerups, and inventory then with the merchant

and used them with cmd inv.

Custom player skins are possible. Not restricted per class.

 Spies can custom skins.

Spies do not show up on auto_id which was a dead giveaway spy checker.
Some admin surely had a boner for spy reduction, just catch them and whack them.
Spies however, do not get a spot player fob marker when undercover. Gun check them.
The Scout can still detect spies.
